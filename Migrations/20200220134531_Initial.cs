﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace TestApsProject.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Name",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    first = table.Column<string>(nullable: true),
                    last = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Name", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "People",
                columns: table => new
                {
                    _id = table.Column<string>(nullable: false),
                    index = table.Column<int>(nullable: false),
                    balance = table.Column<string>(nullable: true),
                    picture = table.Column<string>(nullable: true),
                    age = table.Column<int>(nullable: false),
                    eyeColor = table.Column<string>(nullable: true),
                    nameId = table.Column<int>(nullable: true),
                    company = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    address = table.Column<string>(nullable: true),
                    about = table.Column<string>(nullable: true),
                    tags = table.Column<List<string>>(nullable: true),
                    favoriteFruit = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_People", x => x._id);
                    table.ForeignKey(
                        name: "FK_People_Name_nameId",
                        column: x => x.nameId,
                        principalTable: "Name",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Friend",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    id = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    Person_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Friend", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Friend_People_Person_id",
                        column: x => x.Person_id,
                        principalTable: "People",
                        principalColumn: "_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Friend_Person_id",
                table: "Friend",
                column: "Person_id");

            migrationBuilder.CreateIndex(
                name: "IX_People_nameId",
                table: "People",
                column: "nameId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Friend");

            migrationBuilder.DropTable(
                name: "People");

            migrationBuilder.DropTable(
                name: "Name");
        }
    }
}
