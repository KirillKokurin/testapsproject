﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApsProject.Services
{
    public class UtcDateTimeHandler : IDateTimeHandler
    {
        public string ConvertDateTimeToString(DateTime dateTime)
        {
            DateTime TimeUtc = dateTime.ToUniversalTime();
            return TimeUtc.ToString("t") + " UTC";
        }
    }
}
