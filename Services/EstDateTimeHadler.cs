﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApsProject.Services
{
    public class EstDateTimeHadler : IDateTimeHandler
    {
        public string ConvertDateTimeToString(DateTime dateTime)
        {
            DateTime TimeUtc = dateTime.ToUniversalTime();
            TimeZoneInfo EasternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime EasternTime = TimeZoneInfo.ConvertTimeFromUtc(TimeUtc, EasternZone);
            return EasternTime.ToString("t") + " EST";
        }
    }
}
