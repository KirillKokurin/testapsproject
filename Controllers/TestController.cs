﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using TestApsProject.Services;

namespace TestApsProject.Controllers
{
    public class TestController : Controller
    {
        private IDateTimeHandler DateTimeHandler;

        public TestController(IDateTimeHandler handler)
        {
            DateTimeHandler = handler;
        }
        public IActionResult Index()
        {
            return View();
        }
        public string GetString()
        {
            return "Hello";
        }
        public string Hello(string name)
        {
            return $"Hello {name}";
        }
        public string GetDayOfYear()
        {
            string DayOfYear = DateTime.Now.DayOfYear.ToString();
            return $"Today is the {DayOfYear} of the year";
        }
        public string GetTime()
        {
            //int hours = DateTime.Now.Hour;
            //string Day = "";
            //if(hours >= 9)
            //{
            //    Day = "Good afternoon";
            //}
            //else if(hours >= 3)
            //{
            //    Day = "Good morning";
            //}
            //else if(hours >= 15)
            //{
            //    Day = "Good evening";
            //}
            //else if(hours >= 21)
            //{
            //    Day = "Good night";
            //}
            //return $"{Day} it is {DateTime.Now.ToString("t")} oclock";
            return DateTimeHandler.ConvertDateTimeToString(DateTime.Now);
        }
        public string Sum(int a, int b)
        {
            return (a + b).ToString();
        }
    }
}