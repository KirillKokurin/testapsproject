﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace TestApsProject.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Hello(string name)
        {
            ViewBag.Name = name;
            return View();
        }
        public IActionResult GetData()
        {
            string json = System.IO.File.ReadAllText(@"Data\sample.json");
            List<Models.Person> people = JsonSerializer.Deserialize<List<Models.Person>>(json);

            return View(people);
        }
    }
}